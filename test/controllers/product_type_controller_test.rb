require 'test_helper'

class ProductTypeControllerTest < ActionController::TestCase
  test "should get name:string" do
    get :name:string
    assert_response :success
  end

  test "should get slug:index" do
    get :slug:index
    assert_response :success
  end

end
