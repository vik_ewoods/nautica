Rails.application.routes.draw do


  mount Ckeditor::Engine => '/ckeditor'

  scope '(:locale)', :locale => /ru|uk/ do

    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

    # Faq
    get 'faq', to: 'faq#index', as: 'faq_index'
    get 'faq/:id', to: 'faq#show', as: 'faq_item'

    # System usage index and show pages
    get 'usage', to: 'publication#usage_index', as: 'usage_index'
    get 'usage/:id', to: 'publication#usage_show', as: 'usage_show'

    # News index and show pages
    get 'news', to: 'publication#news_index', as: 'news_index'
    get 'news/:id', to: 'publication#news_show', as: 'news_show'

    # Articles index and show pages
    get 'articles', to: 'publication#articles_index', as: 'articles_index'
    get 'articles/:id', to: 'publication#articles_show', as: 'articles_show'

    # Work out for products
    get 'equipments', to: 'product#index', as: 'product_index'
    get 'equipments/:product_type_id', to: 'product#by_type', as: 'product_by_type'
    get 'equipments/:product_type_id/:product_brand_id', to: 'product#by_brand', as: 'product_by_brand'
    get 'equipments/:product_type_id/:product_brand_id/:id', to: 'product#by_id', as: 'product_by_id'


    # Static pages
    get 'terms', to: 'page#terms', as: 'terms'
    get 'about-system', to: 'page#about_system', as: 'about_system'
    get 'about-us', to: 'page#about_us', as: 'about_us'
    get 'economy-effect', to: 'page#economy_effect', as: 'economy_effect'
    get 'page-not-found', to: 'page#error_404', as: 'page_not_found'
    get 'sitemap', to: 'page#sitemap', as: 'sitemap'

    get 'condition', to: 'page#condition', as: 'condition'

    get 'contact-us', to: 'contact#new', as: 'contact'
    get 'call-back', to: 'contact#feedback', as: 'feedback'
    #post 'contact-us', to: 'contact#create', as: 'contact'
    #resource :contacts, only: [:new, :create]
    #match 'contact-us' => 'contact#new', :as => 'contact', :via => :get
    #match 'contact-us' => 'contact#create', :as => 'contact', :via => :post

    # Root patch
    root 'page#index'

  end

end
