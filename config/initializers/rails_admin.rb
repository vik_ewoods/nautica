#require 'i18n'
#I18n.default_locale = :ru
#I18n.enforce_available_locales = false

RailsAdmin.config do |config|



  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.included_models = [ Publication, Faq, Catalog::Product, Catalog::ProductBrand, Feedback, HomeBanner, 'Publication::Translation', 'Catalog::ProductBrand::Translation', 'Catalog::Product::Translation', 'Faq::Translation', StaticPageData, 'StaticPageData::Translation' ]

end
