# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141003163511) do

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "faq_translations", force: true do |t|
    t.integer  "faq_id",     null: false
    t.string   "locale",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "question"
    t.text     "answer"
  end

  add_index "faq_translations", ["faq_id"], name: "index_faq_translations_on_faq_id"
  add_index "faq_translations", ["locale"], name: "index_faq_translations_on_locale"

  create_table "faqs", force: true do |t|
    t.string   "slug"
    t.string   "question"
    t.text     "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "faqs", ["slug"], name: "index_faqs_on_slug"

  create_table "feedbacks", force: true do |t|
    t.string   "admin_email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "home_banners", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "link"
  end

  create_table "product_brand_translations", force: true do |t|
    t.integer  "product_brand_id", null: false
    t.string   "locale",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "product_brand_translations", ["locale"], name: "index_product_brand_translations_on_locale"
  add_index "product_brand_translations", ["product_brand_id"], name: "index_product_brand_translations_on_product_brand_id"

  create_table "product_brands", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_type_id"
  end

  add_index "product_brands", ["slug"], name: "index_product_brands_on_slug"

  create_table "product_translations", force: true do |t|
    t.integer  "product_id",        null: false
    t.string   "locale",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "header"
    t.text     "short_description"
    t.text     "full_description"
    t.text     "tech_spec"
    t.text     "interface_spec"
    t.text     "feature_spec"
    t.text     "additional_spec"
  end

  add_index "product_translations", ["locale"], name: "index_product_translations_on_locale"
  add_index "product_translations", ["product_id"], name: "index_product_translations_on_product_id"

  create_table "product_type_translations", force: true do |t|
    t.integer  "product_type_id", null: false
    t.string   "locale",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "product_type_translations", ["locale"], name: "index_product_type_translations_on_locale"
  add_index "product_type_translations", ["product_type_id"], name: "index_product_type_translations_on_product_type_id"

  create_table "product_types", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_types", ["slug"], name: "index_product_types_on_slug"

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "header"
    t.text     "short_description"
    t.text     "full_description"
    t.string   "slug"
    t.integer  "product_brand_id"
    t.text     "tech_spec"
    t.text     "interface_spec"
    t.text     "feature_spec"
    t.text     "additional_spec"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "products", ["slug"], name: "index_products_on_slug"

  create_table "publication_catalogs", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "publication_translations", force: true do |t|
    t.integer  "publication_id",    null: false
    t.string   "locale",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.text     "short_description"
    t.text     "full_description"
  end

  add_index "publication_translations", ["locale"], name: "index_publication_translations_on_locale"
  add_index "publication_translations", ["publication_id"], name: "index_publication_translations_on_publication_id"

  create_table "publications", force: true do |t|
    t.boolean  "published"
    t.integer  "publication_catalog_id"
    t.string   "name"
    t.string   "short_description"
    t.text     "full_description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "avatar_file_name"
    t.integer  "avatar_file_size"
    t.string   "avatar_content_type"
    t.datetime "avatar_updated_at"
    t.string   "meta_description"
    t.string   "meta_keywoard"
  end

  add_index "publications", ["slug"], name: "index_publications_on_slug"

  create_table "static_page_data", force: true do |t|
    t.string   "url"
    t.string   "body_title"
    t.string   "head_title"
    t.text     "meta_keywords"
    t.text     "meta_description"
    t.integer  "has_static_page_data_id"
    t.string   "has_static_page_data_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "static_page_datum_translations", force: true do |t|
    t.integer  "static_page_datum_id", null: false
    t.string   "locale",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "head_title"
    t.text     "meta_keywords"
    t.text     "meta_description"
  end

  add_index "static_page_datum_translations", ["locale"], name: "index_static_page_datum_translations_on_locale"
  add_index "static_page_datum_translations", ["static_page_datum_id"], name: "index_static_page_datum_translations_on_static_page_datum_id"

end
