class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :admin_email

      t.timestamps
    end
  end
end
