class AddAvatarToPublication < ActiveRecord::Migration
  def change
    add_column :publications, :avatar_file_name, :string
    add_column :publications, :avatar_file_size, :integer
    add_column :publications, :avatar_content_type, :string
    add_column :publications, :avatar_updated_at, :datetime
  end
end
