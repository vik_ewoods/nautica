class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.boolean :published
      t.integer :publication_catalog_id
      t.string :name
      t.string :short_description
      t.text :full_description

      t.timestamps
    end
  end
end
