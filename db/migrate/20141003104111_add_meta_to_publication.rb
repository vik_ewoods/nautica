class AddMetaToPublication < ActiveRecord::Migration
  def change
    add_column :publications, :meta_description, :string
    add_column :publications, :meta_keywoard, :string
  end
end
