class CreatePublicaionTranslations < ActiveRecord::Migration
  def up
    Publication.create_translation_table! :name => :string, :short_description => :text, :full_description => :text
  end

  def down
    Publication.drop_translation_table!
  end
end
