class AddTranslationToProductBrand < ActiveRecord::Migration
  def up
    ProductBrand.create_translation_table! :name => :string
  end

  def down
    ProductBrand.drop_translation_table!
  end
end
