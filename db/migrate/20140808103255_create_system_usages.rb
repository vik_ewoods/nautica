class CreateSystemUsages < ActiveRecord::Migration
  def change
    create_table :system_usages do |t|
      t.boolean :published
      t.string :name
      t.string :short_description
      t.string :full_description

      t.timestamps
    end
  end
end
