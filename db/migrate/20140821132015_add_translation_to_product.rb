class AddTranslationToProduct < ActiveRecord::Migration
  def up
    Catalog::Product.create_translation_table! :name => :string, :header => :string, :short_description => :text, :full_description => :text, :tech_spec => :text, :interface_spec => :text, :feature_spec => :text, :additional_spec => :text
  end

  def down
    Catalog::Product.drop_translation_table!
  end
end
