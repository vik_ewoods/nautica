class CreatePublicationCatalogs < ActiveRecord::Migration
  def change
    create_table :publication_catalogs do |t|
      t.string :name

      t.timestamps
    end
  end
end
