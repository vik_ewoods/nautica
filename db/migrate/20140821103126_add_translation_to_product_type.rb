class AddTranslationToProductType < ActiveRecord::Migration
  def up
    ProductType.create_translation_table! :name => :string
  end

  def down
    ProductType.drop_translation_table!
  end
end
