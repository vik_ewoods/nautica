class CreateFaqs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.string :slug
      t.string :question
      t.text :answer

      t.timestamps
    end
    add_index :faqs, :slug
  end
end
