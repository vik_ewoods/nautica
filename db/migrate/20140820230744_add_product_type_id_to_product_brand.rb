class AddProductTypeIdToProductBrand < ActiveRecord::Migration
  def change
    add_column :product_brands, :product_type_id, :integer
  end
end
