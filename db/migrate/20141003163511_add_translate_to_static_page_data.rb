class AddTranslateToStaticPageData < ActiveRecord::Migration
  def up
    StaticPageData.create_translation_table!
  end

  def down
    StaticPageData.drop_translation_table!
  end
end
