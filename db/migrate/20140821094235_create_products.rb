class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :header
      t.text :short_description
      t.text :full_description
      t.string :slug
      t.integer :product_brand_id
      t.text :tech_spec
      t.text :interface_spec
      t.text :feature_spec
      t.text :additional_spec

      t.timestamps
    end
    add_index :products, :slug
  end
end
