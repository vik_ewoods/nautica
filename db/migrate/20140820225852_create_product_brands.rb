class CreateProductBrands < ActiveRecord::Migration
  def change
    create_table :product_brands do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
    add_index :product_brands, :slug
  end
end
