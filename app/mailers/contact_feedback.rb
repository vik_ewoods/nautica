class ContactFeedback < ActionMailer::Base



  default from: "Support <support@voroninstudio.eu>"
  default to: "vik.ewoods@gmail.com"


  def send_feedback(feedback_data)
    @feedback = feedback_data
    to = []
    to = Feedback.first.feedback.split(',')
    mail(:template_path => 'contact_feedback', :template_name => 'send_feedback', :subject => "Форма зворотнього звязку! Nautica", to: to)
  end

end
