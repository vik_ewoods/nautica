class ContactController < ApplicationController
  def new
    @page_title = "Контакты"
    add_breadcrumb " ", :root_path
    add_breadcrumb @page_title
  end

  def create
    @page_title = "Контакты"
    add_breadcrumb " ", :root_path
    add_breadcrumb @page_title
  end

  def feedback
    #ContactFeedback.send
    render layout: 'call_back_layouts'
  end
end
