class ProductController < ApplicationController

  add_breadcrumb " ", :root_path

  def index
    if params[:search]
      @products = Catalog::Product.where("name like ?", "%#{params[:search]}%").paginate(:page => params[:page], :per_page => 9)

      @page_title = I18n.t('system_elements.unsorted.search')
      add_breadcrumb @page_title

      if @products.empty?
        render template: "product/empty_search"
      end

      add_breadcrumb params[:search]
    else
      @products = Catalog::Product.paginate(:page => params[:page])
      @page_title = I18n.t('system_elements.breadcrumbs.equipment')
      add_breadcrumb @page_title
      render template: "product/catalog"
    end
  end

  def show
  end

  def by_type
    @products_type ||= Catalog::ProductType.find_by_slug!(params[:product_type_id])
    @products = @products_type
    @page_title = @products_type.name
    add_breadcrumb I18n.t('system_elements.breadcrumbs.equipment'), :product_index_path
    add_breadcrumb @products_type.name
  end

  def by_brand
    @products_brand ||= Catalog::ProductBrand.find_by_slug!(params[:product_brand_id])
    @all_brands = Catalog::ProductBrand.where("product_type_id like ? ", "%#{@products_brand.product_type.id}%")
    @products = @products_brand
    @page_title = @products_brand.name
    #add_breadcrumb I18n.t('system_elements.breadcrumbs.equipment'), :product_index_path
    add_breadcrumb @products_brand.product_type.name, product_by_type_path(:product_type_id => @products_brand.product_type.slug)
    add_breadcrumb @products_brand.name
  end

  def by_id
    @product ||= Catalog::Product.find_by_slug!(params[:id])
    @page_title = @product.name
    #add_breadcrumb I18n.t('system_elements.breadcrumbs.equipment'), :product_index_path
    add_breadcrumb @product.product_brand.product_type.name, product_by_type_path(:product_type_id => @product.product_brand.product_type.slug)
    add_breadcrumb @product.product_brand.name, product_by_brand_path(:product_brand_id => @product.product_brand.slug)
    add_breadcrumb @page_title
  end



end
