class FaqController < ApplicationController

  add_breadcrumb " ", :root_path

  def index
    @faqs = Faq.all
    @page_title = I18n.t('system_elements.breadcrumbs.faq')
    add_breadcrumb @page_title
  end

  def show
  end
end
