class PublicationController < ApplicationController

  add_breadcrumb " ", :root_path

  def index
  end

  def show
  end

  def usage_index
    @page_title = I18n.t('system_elements.breadcrumbs.usage')
    add_breadcrumb @page_title
    @usages ||= PublicationCatalog.find(4)
  end

  def usage_show
    @usages = Publication.find_by_slug!(params[:id])
    @page_title = I18n.t('system_elements.breadcrumbs.usage')
    add_breadcrumb I18n.t('system_elements.breadcrumbs.usage'), :usage_index_path
    add_breadcrumb @usages.name
  end

  def news_index
    @page_title = I18n.t('system_elements.breadcrumbs.news')
    @news ||= PublicationCatalog.find(1)
    add_breadcrumb @page_title
  end

  def news_show
    @news = Publication.find_by_slug!(params[:id])
    @news_f = PublicationCatalog.find(1)
    #@news_f_d = @news_f.last(4)
    @page_title = I18n.t('system_elements.breadcrumbs.news')
    add_breadcrumb I18n.t('system_elements.breadcrumbs.news'), :news_index_path
    add_breadcrumb @news.name
  end

  def articles_index
    @page_title = I18n.t('system_elements.breadcrumbs.articles')
    @articles ||= PublicationCatalog.find(2)
    add_breadcrumb @page_title
  end

  def articles_show
    @articles = Publication.find_by_slug!(params[:id])
    @page_title = I18n.t('system_elements.breadcrumbs.articles')
    add_breadcrumb I18n.t('system_elements.breadcrumbs.articles'), :articles_index_path
    add_breadcrumb @articles.name
  end
end
