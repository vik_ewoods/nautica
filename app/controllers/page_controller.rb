class PageController < ApplicationController

	def index
    @page_title = ''
    @system_usage_home ||= PublicationCatalog.find(4)
    @news_home ||= PublicationCatalog.find(1)
    @article_home ||= PublicationCatalog.find(2)
    @banners = HomeBanner.last(3)
  end

	def terms
		@page_title = I18n.t('system_elements.footer.menu.terms')
    add_breadcrumb " ", :root_path
    add_breadcrumb @page_title
	end

	def about_system
		@page_title = I18n.t('system_elements.header_nav.ability')
    add_breadcrumb " ", :root_path
    add_breadcrumb @page_title
	end

	def about_us
		@page_title = I18n.t('system_elements.footer.menu.abouts_us')
    add_breadcrumb " ", :root_path
    add_breadcrumb @page_title
	end

	def economy_effect
		@page_title = I18n.t('system_elements.header_nav.economy_effect')
    add_breadcrumb " ", :root_path
    add_breadcrumb @page_title
	end

	def error_404
		@page_title = ''
  end

  def condition

  end


  def call_back

  end


  def self.search(query)
    where("name like ?", "%#{query}%")
  end

end
