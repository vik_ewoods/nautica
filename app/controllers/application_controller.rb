class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale
  helper_method :get_static

  def get_static(id)
    @static = Publication.find(id)
  end



  private
  def browser_lang
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  def set_locale

    # if params[:controller].index('devise').nil? && params[:controller].index('rails_admin').nil?
    #   locale = params[:locale]
    #   if !locale
    #     locale = http_accept_language.compatible_language_from(I18n.available_locales)
    #   end
    #
    #   if !locale
    #     locale = I18n.default_locale
    #   end
    #
    #   if params[:locale] != locale
    #     redirect_to locale: locale
    #   else
    #     I18n.locale = locale
    #   end
    # end
    if params[:controller].index('devise').nil? && params[:controller].index('rails_admin').nil?
      if (browser_lang == 'ru') && (browser_lang == 'uk')
        I18n.locale = params[:locale] || browser_lang
      else
        I18n.locale = params[:locale] || I18n.default_locale
      end


      Rails.application.routes.default_url_options[:locale] = I18n.locale
    else
      I18n.locale = :en
      Rails.application.routes.default_url_options[:locale] = I18n.locale
    end
  end

end
