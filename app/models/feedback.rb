class Feedback < ActiveRecord::Base
  attr_accessible :admin_email

  rails_admin do
    navigation_label 'Електронная почта'
    label 'Електронная почта'
    label_plural 'Електронная почта'

    list do
      field :admin_email do
        label "Форма обратной связи"
      end
    end

    edit do
      field :admin_email do
        label "Форма обратной связи"
        help ''
      end
    end
  end
end
