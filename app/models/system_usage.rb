class SystemUsage < ActiveRecord::Base

  has_one :static_page_data, :as => :has_static_page_data
  accepts_nested_attributes_for :static_page_data, :allow_destroy => true

	rails_admin do
		list do
			field :published
			field :name
			field :short_description
			field :created_at do
				date_format :short
			end
		end

		edit do
			field :published
			field :name
			field :short_description, :text do
				html_attributes style: "height: 180px; width: 400px;"
			end
			field :full_description, :ck_editor do
				#config_options :html => true
      end
      field :static_page_data
		end
	end
end
