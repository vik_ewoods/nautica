class Faq < ActiveRecord::Base
  attr_accessible :question, :answer, :slug, :translations, :translations_attributes

  default_scope { order('created_at DESC') }

  translates :question, :answer

  accepts_nested_attributes_for :translations, allow_destroy: true

  has_one :static_page_data, :as => :has_static_page_data
  accepts_nested_attributes_for :static_page_data, :allow_destroy => true


  class Translation
    attr_accessible :locale, :faq_id, :question, :answer

    rails_admin do
      visible false
      nested do
        field :locale do
          visible false
        end
        field :question do
          label 'Вопрос'
          help ''
        end
        field :answer do
          label 'Ответ'
          help ''
        end
      end
    end
  end



  before_validation :friendly_url_faq

  validates :question, presence: true, uniqueness: true
  validates :slug, presence: true, uniqueness: true
  validates :answer, presence: true

  def to_param
    slug
  end

  def friendly_url_faq
    self.slug = question.parameterize
  end

  rails_admin do
    list do
      field :question
    end
    edit do
      field :question
      field :answer
      field :translations, :globalize_tabs
      field :static_page_data
    end
  end

end
