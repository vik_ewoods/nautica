# encoding: UTF-8
class Publication < ActiveRecord::Base

  # Model attributes
  attr_accessible :published, :name, :short_description, :full_description, :slug, :translations, :translations_attributes, :avatar, :publication_catalog, :publication_catalog_id, :meta_description, :meta_keywoard, :static_page_data, :static_page_data_attributes


  # Model belongs to publication catalog
  belongs_to :publication_catalog

  # Before saving generate url
  before_validation :friendly_url

  has_one :static_page_data, :as => :has_static_page_data
  accepts_nested_attributes_for :static_page_data, :allow_destroy => true

  # Defualt sorting of elemts from DB
  default_scope { order('created_at DESC') }

  # Translation section
  translates :name, :short_description, :full_description, :fallbacks_for_empty_translations => true
  # Translation class
  class Translation
    attr_accessible :locale, :publication_id, :name, :short_description, :full_description, :publication_catalog, :publication_catalog_id

    rails_admin do
      visible false
      nested do
        field :locale, :hidden
        field :name do
          label 'Название'
          help ''
        end
        field :short_description do
          label 'Краткое описание'
          help ''
        end
        field :full_description, :ck_editor do
          label 'Содержание'
          help ''
        end
      end
    end
  end

  accepts_nested_attributes_for :translations

  # Model validation of records
  validates :name, presence: true, length: {maximum: 65}, allow_blank: false
  validates :slug, uniqueness: true, presence: true, allow_blank: false
  validates :short_description, presence: true, length: {maximum: 130}, allow_blank: false
  validates :publication_catalog, presence: true


  has_attached_file :avatar, styles:{
                                usage_avatar: '160x260#',
                                news_avatar: '130x130#',
                                news_inside: '300x300#',
                                article_avatar: '490x85#',
                                article_page_banner: '1020x400#'
                                },
                                url:'/assets/images/publications/:class/:id/image_:style.:extension',
                                path:':rails_root/public:url',
                                default_url: '/assets/images/no_image_:style.jpg'

  validates_attachment :avatar, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }


  def to_param
    slug
  end

  def friendly_url
    self.slug = name.parameterize
  end

  rails_admin do

    list do
      field :published
      field :publication_catalog
      field :name
    end

    edit do
      field :published
      field :publication_catalog do
        inline_add false
        inline_edit false
      end
      field :avatar, :paperclip
      field :name
      field :short_description, :text do
        html_attributes style: "height: 180px; width: 400px;"
      end
      field :full_description, :ck_editor do

      end
      field :static_page_data
      field :slug do

      end
      field :translations, :globalize_tabs


    end
  end
end
