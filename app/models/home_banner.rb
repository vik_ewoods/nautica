class HomeBanner < ActiveRecord::Base

  attr_accessible :name, :description, :avatar, :link


  has_attached_file :avatar, styles:{
      prew: '100x100#',
  },
                    url:'/assets/images/publications/:class/:id/image_:style.:extension',
                    path:':rails_root/public:url',
                    default_url: '/assets/images/no_image_:style.jpg'

  validates_attachment :avatar, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }



  rails_admin do
    label 'Баннер'
    label_plural 'Баннера'

    list do
      field :name
      field :description
    end

    edit do
      field :name
      field :description
      field :link
      field :avatar, :paperclip
    end

  end
end
