class PublicationCatalog < ActiveRecord::Base
  has_many :publications
end
