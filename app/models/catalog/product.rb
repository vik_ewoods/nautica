class Catalog::Product < ActiveRecord::Base
  attr_accessible :name, :header, :short_description, :full_description, :slug, :product_brand_id, :product_brand, :tech_spec, :interface_spec, :feature_spec, :additional_spec, :translations, :translations_attributes, :avatar
  belongs_to :product_brand
  before_validation :generate_uri
  default_scope { order('created_at DESC') }

  has_one :static_page_data, :as => :has_static_page_data
  accepts_nested_attributes_for :static_page_data, :allow_destroy => true

  translates :name, :header, :short_description, :full_description, :tech_spec, :interface_spec, :feature_spec, :additional_spec, :fallbacks_for_empty_translations => true

  class Translation
    attr_accessible :locale, :name, :header, :short_description, :full_description, :tech_spec, :interface_spec, :feature_spec, :additional_spec, :product_brand_id, :product_brand
    rails_admin do
      visible false
      nested do
        field :locale, :hidden
        field :name do
          label 'Название'
          help ''
        end
        field :header do
          label 'Заголовок'
          help ''
        end
        field :short_description do
          label 'Краткое описание'
          help ''
        end
        field :full_description, :ck_editor do
          label 'Содержание'
          help ''
        end
        field :tech_spec, :ck_editor do
          label 'Технические спецификации'
          help ''
        end
        field :interface_spec, :ck_editor do
          label ''
          help ''
        end
        field :feature_spec, :ck_editor do
          label ''
          help ''
        end
        field :additional_spec, :ck_editor do
          label ''
          help ''
        end
      end
    end
  end

  accepts_nested_attributes_for :translations
  validates :name, presence: true, allow_blank: false
  validates :header, presence: true, allow_blank: false
  validates :short_description, presence: true, allow_blank: false
  validates :slug, presence: true, allow_blank: false, uniqueness: true
  validates :product_brand, presence: true

  has_attached_file :avatar, styles:{
      main: '265x180^'
  },
                    url:'/assets/images/equipment/:class/:id/image_:style.:extension',
                    path:':rails_root/public:url'

  validates_attachment :avatar, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }

  def to_param
    slug
  end

  def generate_uri
    self.slug = name.parameterize
  end

  rails_admin do
    list do
      field :avatar
      field :name
      field :product_brand
    end
    edit do
      field :avatar, :paperclip
      field :product_brand do
        inline_add false
        inline_edit false
      end
      field :name
      field :slug do
        visible false
      end
      field :header
      field :short_description
      field :full_description, :ck_editor
      field :tech_spec, :ck_editor
      field :interface_spec, :ck_editor
      field :feature_spec, :ck_editor
      field :additional_spec, :ck_editor
      field :translations, :globalize_tabs
      field :static_page_data
    end
  end
end
