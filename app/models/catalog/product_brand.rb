class Catalog::ProductBrand < ActiveRecord::Base
  # Attrebute of model
  attr_accessible :name, :product_type, :product_type_id, :slug, :translations, :translations_attributes
  # Model relations
  belongs_to :product_type
  has_many :products
  has_one :static_page_data, :as => :has_static_page_data
  accepts_nested_attributes_for :static_page_data, :allow_destroy => true
  # Generate uri
  before_validation :friendly_uri
  # Default sorting
  default_scope {order('created_at DESC')}
  # Translations
  translates :name, :fallbacks_for_empty_translations => true
  class Translation
    attr_accessible :locale, :product_brand_id, :name

    rails_admin do
      visible false
      nested do
        field :locale, :hidden
        field :name do
          label 'Название'
          help ''
        end
        #field :product_type
      end
    end
  end

  accepts_nested_attributes_for :translations

  validates :name, presence: true, allow_blank: false
  validates :slug, presence: true, allow_blank: false
  validates :product_type, presence: true

  def to_param
    slug
  end

  def friendly_uri
    self.slug = name.parameterize
  end

  rails_admin do
    list do
      field :name do
        label 'Название'
      end
      field :product_type do
        label 'Тип оборудования'
      end
    end
    edit do
      field :name do
        label 'Название'
      end
      field :product_type do
        inline_add false
        inline_edit false
        label 'Тип оборудования'
      end
      field :products, :hidden do
        label ''
        help ''
        visible false
      end
      field :translations, :globalize_tabs do
        label 'Перевод'
        #help 'Перевод текста на 3 язык, русский заполнятся автоматически при первом заполнении'
      end
      field :static_page_data
    end
  end
end
