class Catalog::ProductType < ActiveRecord::Base
  #set_table_name "product_types"
  # Model fields
  attr_accessible :name, :slug, :product_brands, :translations, :translations_attributes
  # Model relations
  has_many :product_brands
  # Defualt sorting from database
  default_scope {order('created_at DESC')}
  # Validation details
  validates :name, presence: true, allow_blank: false
  validates :slug, presence: true, uniqueness: true
  # Before validates
  before_validation :friendly_url
  # Translate
  translates :name, :fallbacks_for_empty_translations => true
  # Translates class
  class Translation
    attr_accessible :locale, :product_type_id, :name

    rails_admin do
      label 'Перевод'
      label_plural 'Перевод'
      visible false
      nested do
        field :locale, :hidden
        field :name do
          label 'Название'
        end
      end
    end
  end
  # Nested for translates
  accepts_nested_attributes_for :translations
  # Generate url
  def to_param
    slug
  end

  def friendly_url
    self.slug = name.parameterize
  end

  rails_admin do
    list do
      field :name
    end
    edit do
      field :name
      field :slug#, :hidden
      field :translations, :globalize_tabs
    end
  end
end
