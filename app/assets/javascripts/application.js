// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require faq
//= require modernizer
//= require nprogress
//= require nprogress-turbolinks
//= require turbolinks-np
//= require unslider
//= require selectordie
//= require jquery.lightSlider
//= require jquery.fancybox
//= require jquery.form_validation
//= require selectize



var initializeGMaps;

initializeGMaps = function() {
  var lat, latlng, lng, map, mapOptions, marker, styledMap, styles;
  lat = 49.810126;
  lng = 23.971897; // 49.810126, 23.971897
  latlng = new google.maps.LatLng(lat, lng);
  styles = [
    {
      featureType: 'all',
      stylers: [
        { "saturation": -95 },
        { "lightness": 1 },
        { "gamma": 0.95 }
      ]
    }
  ];
  styledMap = new google.maps.StyledMapType(styles, {
    name: "Styled Map"
  });
  mapOptions = {
    zoom: 16,
    center: latlng,
    scrollwheel: false,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, "map_style"]
    }
  };

    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading" style="text-align: center;">Наутика</h1>'+
        '<div id="bodyContent" style="text-align: center;">'+
            '<img src="/assets/logo.png" style="width: 100px">'+
        '<p>' +
        'вул. Щирецька, 36, оф. 205 <br />'+
        'м. Львів, Україна <br />'+
        '<br />'+
        '+38 (067) 340-26-13 <br />'+
        '+38 (032) 295-72-96 <br />'+
        '</p>'+
        '</div>'+
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });


  map = new google.maps.Map(document.getElementById("g_map"), mapOptions);
  map.mapTypes.set("map_style", styledMap);
  map.setMapTypeId("map_style");
  marker = new google.maps.Marker({
    position: latlng,
    map: map,
    title: "Наутика",
    icon: '/assets/ico-map.png'
  });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });
};


$.fn.tabs = function() {
    var selector = this;

    this.each(function() {
        var obj = $(this);

        $(obj.attr('href')).hide();

        $(obj).click(function() {
            $(selector).removeClass('selected');

            $(selector).each(function(i, element) {
                $($(element).attr('href')).hide();
            });

            $(this).addClass('selected');

            $($(this).attr('href')).fadeIn();

            return false;
        });
    });

    $(this).show();

    $(this).first().click();
};


$(document).on('ready page:load', function () {

	var $body = $('body');
	var $setting_header = $('#main-navigation');
	var $logo = $('section#logo');
	var $screen_width = $(window).width();


    $('#select-beast-empty').selectize({
        //allowEmptyOption: true,
        //create: true
    });


    if(navigator.userAgent.match(/Firefox/i)){
        $body.addClass('firefox');
        console.log('This is firefox!');
    }else if(navigator.userAgent.match(/Opera/i)){
        $body.addClass('opera');
    }


    $.validate({
        form : '#f_call_back, #ask-question-form, #contact-question-form',
        validateOnBlur : false,
        //modules : 'security',
        onError : function() {
            alert('Validation failed');
        },
        onSuccess : function() {
            alert('The form is valid!');
            return false; // Will stop the submission of the form
        },
        onValidate : function() {
            return {
                element : $('#some-input'),
                message : 'This input has an invalid value for some reason'
            }
        }
    });


    $("#call-back-btn").fancybox({
        maxWidth	: 960,
        maxHeight	: 380,
        fitToView	: false,
        width		: '960px',
        height		: '380px',
        autoSize	: false,
        closeClick	: false,
        closeBtn : true,
        openEffect	: 'none',
        closeEffect	: 'none',
        padding: 0,
        margin:0
    });

    $("#call-back-contactpage").fancybox({
        maxWidth	: 960,
        maxHeight	: 380,
        fitToView	: false,
        width		: '960px',
        height		: '380px',
        autoSize	: false,
        closeClick	: false,
        closeBtn : true,
        openEffect	: 'none',
        closeEffect	: 'none',
        padding: 0,
        margin:0
    });


    $("#product_features_tabs a").click(function(event) {
        event.preventDefault();
        $(this).addClass("active_tab_selected");
        $(this).siblings().removeClass("active_tab_selected");
        var tab = $(this).data();
        //console.log("#"+tab.data("tabs"));
        $(".tab-content").not(tab).css("display", "none");
        $("#"+$(this).data('tabs')).fadeIn("fast");
    });


    $("#main_index_page_slider_content").lightSlider({
        slideWidth:1020,
        slideMargin:0,
        slideMove:1,
        minSlide:1,
        maxSlide:3,

        pager:true,
        controls:true,
        prevHtml:'',
        nextHtml:'',
        keyPress:true,
        thumbWidth:50,
        thumbMargin:3,
        gallery:false,
        currentPagerPosition:'middle',

        useCSS:true,
        auto: false,
        pause: 2000,
        loop:true,
        easing: '',
        speed: 1000,
        mode:"slide",
        swipeThreshold:10,

        onBeforeStart: function(){},
        onSliderLoad: function() {},
        onBeforeSlide:function(){},
        onAfterSlide:function(){},
        onBeforeNextSlide: function(){},
        onBeforePrevSlide: function(){}
    });

    $("#active-article-banner").lightSlider({
        slideWidth:1020,
        slideMargin:0,
        slideMove:1,
        minSlide:1,
        maxSlide:3,

        pager:true,
        controls:false,
        prevHtml:'',
        nextHtml:'',
        keyPress:true,
        thumbWidth:50,
        thumbMargin:3,
        gallery:false,
        currentPagerPosition:'middle',

        useCSS:true,
        auto: false,
        pause: 2000,
        loop:true,
        easing: '',
        speed: 1000,
        mode:"slide",
        swipeThreshold:10,

        onBeforeStart: function(){},
        onSliderLoad: function() {},
        onBeforeSlide:function(){},
        onAfterSlide:function(){},
        onBeforeNextSlide: function(){},
        onBeforePrevSlide: function(){}
    });


    var unslider = $('.slider').unslider({
        speed: 750,               //  The speed to animate each slide (in milliseconds)
        delay: 3000,              //  The delay between slide animations (in milliseconds)
        complete: function() {},  //  A function that gets called after every slide animation
        keys: true,               //  Enable keyboard (left, right) arrow shortcuts
        dots: true,               //  Display dot navigation
        fluid: true              //  Support responsive design. May break non-responsive designs
    });

    $('.slider-controll .left').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('next')) {
            unslider.data('unslider')['next']();
        } else {
            unslider.data('unslider')['prev']();
        };
    });

    $('.slider-controll .right').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('next')) {
            unslider.data('unslider')['next']();
        } else {
            unslider.data('unslider')['prev']();
        };
    });

    $('#__to__top').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 1350);
        return false;
    });


  $("#equipment_nav").on({
    mouseenter: function(){
      $("#sub_nav_main").show();
      //$("#eq-li").addClass('active_item');
    },
    mouseleave: function () {
      $("#sub_nav_main").hide();
      //$("#eq-li").removeClass('active_item');
    }
  });


  $("#sub_nav_main").on({
    mouseenter: function(){
      $("#sub_nav_main").show();
      //$("#eq-li").addClass('active_item');
    },
    mouseleave: function () {
      $("#sub_nav_main").hide();
      //$("#eq-li").removeClass('active_item');
    }
  });



  $('#navigation-menu').click(function(event){
    event.preventDefault();

    var $el = $("#sub-navigation").height();
    console.log($el);

    if($el == 4){
      $("#sub-navigation").css('height', '110px');
      //$('.nav_act').css('top', '108px');
      $("#navigation-menu").addClass('active_r');
    }else{
      $("#sub-navigation").css('height', '5px');
      $("#navigation-menu").removeClass('active_r');
      //$('.nav_act').css('top', '13px');
    }


  });

    $("#open-login").click(function(e){
        e.preventDefault();
        $("#header_search").css('width', '0px');
        setTimeout( function() {
            $("#header_login").css('width', '410px');
            $("#open-login").css('float', 'left');
        }, 800);
    });

  $("#open-seacrh").click(function (event) {
    event.preventDefault();

    var $ell = $("#header_search").width();
    console.log($ell);
    if($ell == 0){
      $("#header_search").css('width', '410px');
      $("#header_login").css('width', '0px');
      $("#open-login").css('float', 'right');
    }else{



      if($("#search_field").val() != ""){
        $("#header_search").submit();
      }else{
        $("#header_search").css('width', '0px');
        setTimeout( function() {
          $("#header_login").css('width', '410px');
            $("#open-login").css('float', 'left');
        }, 800);
      }

    }





  });





  initializeGMaps();











});


google.maps.event.addDomListener(window, "ready page:load", initializeGMaps);



