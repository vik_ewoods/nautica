module ApplicationHelper
	def ve_development_tag
		[
				tag('meta', name: 'studio', content: 'Voronin Studio (c) 2014'),
				tag('meta', name: 'developer', content: 'Vik Ewoods'),
        tag('meta', name: 'art-director', content: 'Nick Voronin'),
				tag('meta', name: 'email', content: 'support@voroninstudio.eu'),
        tag('meta', name: 'og:locale', content: 'ru_RU'),
        tag('meta', name: 'og:locale:alternate', content: 'uk_UK'),
		].join("\n").html_safe
  end

  def disable_seo
    [
        tag('meta', name: 'robots', content: 'noindex, nofollow'),
        tag('meta', name: 'MobileOptimized', content: '1020'),
        tag('meta', name: 'HandheldFriendly', content: 'false'),
        tag('meta', name: 'revised', content: Date.today.to_datetime)
    ].join("\n").html_safe
  end

  def edge_ie
    [
        tag('meta', name: 'http-equiv', content: 'IE=edge,chrome=1')
    ].join("\n").html_safe
  end
end
